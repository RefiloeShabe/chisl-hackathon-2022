# Chisl RL Hackathon

You have been tasked with building a systematic trading strategy using a reinforcement learning agent. At the end of each trading period (training, validation, and test) the Sharpe Ratio (a function of networth) of the trading strategy is used to see how well the algorithm did.

**The objective of the challenge:**
> To acquire the highest Sharpe Ratio with a minimal return hurdle being met (i.e. beat the risk free rate of 5%).

Guillaume and Reuben from [Chisl](https://www.chislgroup.com/) will be supporting the hackathon providing technical support and guidance where needed.[Chisl](https://www.chislgroup.com/) is a niche management consultancy that specialises in providing bespoke and innovative solutions for quantitative and data-related problems.

## Background

In finance a common problem is portfolio allocation. Give a universe of investable assets, what is the best way to allocate finite capital to these assets over a period of time in order to maximise risk-adjusted return. This is an *intertemporal choice* problem. Reinforcement learning (RL) solves a sequential decision making process which makes using RL techniques in finance a natural application. 

In this hackathon an RL agent should be trained on a trading environment. The reward function is setup to incentivise the agent to allocate capital to strong performing assets with low volatility (risk). A suitable RL agent should be developed and trained that can approximate the value function (stocks future performance) and determine an optimal policy of portfolio allocation (given this value estimate). 

A trained RL agent should be able to identify stocks which will underperform, have low risk and with future upside. The agent should then be able to long (assign positive weights) to the upside and short (assign negative wieghts) to the underperformers.

## Getting Started

There is a `walkthrough` notebook which provides a high-level example of interacting with the trading environment. It also provides details on `zipping` your code and model and submitting it.

- [ ] `agent.py` - You will **add your model code to this file** (and loaded trained model if applicable). The `agent.py` contains a class which is used to interact with the environment (i.e. take the optimal actions and receive relevant observations). There is a lot of freedom in the design and development of the RL model. You can pretty much do anything in this file, however crucialy the RL **model actions should be added [here](https://gitlab.com/quantalytics-ai/chisl-hackathon-2022/-/blob/main/agent.py#L30).**

- [ ] `hacking_env.py` - This contains the trading environment class. This class **should not be changed in anyway**. You can look at the methods and properties to understand how the environment works and what reward signal is generated. It is worth playing around with the environment to observe what actions your model should be taking and the type of observations it receives. **For advanced use cases you can add more features for your models value / policy functions**. This can be done by calling `_get_info` [here](https://gitlab.com/quantalytics-ai/chisl-hackathon-2022/-/blob/main/hacking_env.py#L208). The prices, net worths and history can all be *transformed* and added as features.

- [ ] `requirements.txt` - The Python libraries our environment will accept. Note we are running Python 3.10.

- [ ] `utils.py` - You can add any boilerplate code here for your models, data wrangling etc.

- [ ] `main.py` - This provides insights into how the code is run on the server for validation and test. You can run this locally to test that your `zipped` files will run on the server.

## Examples

There is a simple example here which just shows how the actions are fed into the decision function of the agent. The jupyter notebook shows how the agent is then called with the environment.
* https://gitlab.com/quantalytics-ai/chisl-hackathon-2022/-/tree/main/agent_example_0

A more complex example using Stable Baselines can be seen here. This illustrates **loading externally saved trained model files**. This is important to get right as it will affect how your submission is run on validation and test.
* https://gitlab.com/quantalytics-ai/chisl-hackathon-2022/-/tree/main/agent_example_1




