import numpy as np
import pandas as pd
import utils

# Random action agent
class Agent:

    def reward_function(
        self,
        navs: list,
    ):
        return utils.gordon(navs)

    def decision_function(
        self,
        observation: dict, 
        prev_reward: int, 
        info: dict,
    ):
        # Add complex logic here to determine what the agent has to do.
        action = np.random.uniform(low=-0.02, high=0.02, size=(50,))
        # Always return action as an np.array of length observation space.
        return action